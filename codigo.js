const button = document.querySelector("#buttonSend");
let corpo = document.querySelector(".areamensagens");

button.addEventListener("click",(event) => {
    var text = document.getElementById("textareabox").value;
    const novaMsg = document.createElement("p");
    novaMsg.innerText = text;
    var btnedit = document.createElement("button");
    var btnerase = document.createElement("button");
    btnedit.innerHTML = "Editar";
    btnedit.className = "botãoeditar"
    btnerase.innerHTML = "Apagar";
    btnerase.className = "botãoapagar"
    var div = document.createElement("div");
    div.appendChild(novaMsg)
    div.appendChild(btnedit)
    div.appendChild(btnerase)
    corpo.insertAdjacentElement("afterbegin",div);
    btnerase.addEventListener("click",(event) => {
        corpo.removeChild(div)
    })
    btnedit.addEventListener("click",(event) => {
        div.children[1].setAttribute("hidden", "true");
        div.children[2].setAttribute("hidden", "true");
        let editText = document.createElement("input");
        div.appendChild(editText)
        let editButton = document.createElement("input");
        editButton.type = "button";
        editButton.value = "Enviar";
        div.appendChild(editButton)
        editButton.addEventListener("click",(event) => {
        function edição(elemento,texto){
            elemento.children[0].innerText = texto
            elemento.children[4].remove();
            elemento.children[3].remove();
            elemento.children[1].removeAttribute("hidden");
            elemento.children[2].removeAttribute("hidden");
            

        }
        edição(div, div.children[3].value)

    })

})})